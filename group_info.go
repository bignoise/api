package api

import "time"

// GroupInfo record
type GroupInfo struct {
	VkID               uint32    `bson:"vk_id" json:"vk_id"`
	SyncWallLastDate   time.Time `bson:"sync_wall_last_date" json:"sync_wall_last_date"`
	WallLastRecordDate time.Time `bson:"wall_last_record_date" json:"wall_last_record_date"`
	MembersCount       int       `bson:"members_count" json:"members_count"`
	MessagesCount      int       `bson:"messages_count" json:"messages_count"`
	Level              float64   `bson:"level"`
	PageRank           float64   `bson:"page_rank"`
	Votes              int       `json:"votes"`
	Rate               float64   `json:"rate"`
	Views              uint32    `json:"views"`
}

package api

const (
	// VoteNotify is vote value when need to notify about event
	VoteNotify = 2
	// VoteNeutrally is cancel of vote
	VoteNeutrally = 0
	// VoteBad is bad
	VoteBad       = -1
	VoteGood       = 1

	NotificationStatusNew    = "new"
	NotificationStatusSended = "sended"
)

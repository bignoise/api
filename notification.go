package api

import (
	"time"

	"gopkg.in/mgo.v2/bson"
)

// Notification record
type Notification struct {
	ID       bson.ObjectId `json:"_id" bson:"_id,omitempty"`
	VkID     uint32        `bson:"vk_id" json:"vk_id"`
	UserID   uint32        `bson:"user_id" json:"user_id"`
	Provider string        `bson:"provider" json:"provider"`
	Date     time.Time     `bson:"date" json:"date"`
	Status   string        `bson:"status" json:"status"`
}

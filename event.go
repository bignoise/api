package api

import (
	"time"

	"bitbucket.org/bignoise/api/vk"
	"gopkg.in/mgo.v2/bson"
)

type EventAnnounce struct {
	ID         bson.ObjectId `json:"id" bson:"_id,omitempty"`
	VkID       int64         `json:"vk_id" bson:"vk_id"`
	Name       string        `json:"name"`
	ScreenName string        `json:"screen_name" bson:"screen_name"`
	Place      vk.Place      `json:"place"`
	Photo      string        `json:"photo" bson:"photo_big"`
	Location   GeoJson       `bson:"location,omitempty" json:"location"`
	Date       time.Time     `json:"date"`
	Level      float64       `json:"level"`
	Vote       Vote          `json:"vote"`
}

type Event struct {
	Community  `bson:",inline"`
	StartDate  time.Time `json:"start_date"`
	FinishDate time.Time `json:"finish_date"`
	Date       time.Time `json:"date"`
	Canceled   bool      `json:"canceled"`
}

package responses

import "bitbucket.org/bignoise/api"

// ResponseCommunities struct
type ResponseCommunities struct {
	Code        int                     `json:"code"`
	Count       int                     `json:"count"`
	Communities []api.CommunityAnnounce `json:"communities"`
}

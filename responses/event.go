package responses

import "bitbucket.org/bignoise/api"

// ResponseEvent struct
type ResponseEvent struct {
	Code  int       `json:"code"`
	Event api.Event `json:"event"`
	Vote  api.Vote  `json:"vote"`
}

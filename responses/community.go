package responses

import "bitbucket.org/bignoise/api"

// CommunityEvent struct
type CommunityEvent struct {
	Code      int           `json:"code"`
	Community api.Community `json:"community"`
	Vote      api.Vote      `json:"vote"`
}

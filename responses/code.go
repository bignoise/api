package responses

// ResponseCode struct
type ResponseCode struct {
	Code int `json:"code"`
}

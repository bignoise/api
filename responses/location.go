package responses

import "bitbucket.org/bignoise/api/vk"

// ResponseLocation struct
type ResponseLocation struct {
	Code     int      `json:"code"`
	Location vk.Place `json:"location"`
}

package responses

import "bitbucket.org/bignoise/api"

// ResponseEvents struct
type ResponseEvents struct {
	Code   int                 `json:"code"`
	Count  int                 `json:"count"`
	Events []api.EventAnnounce `json:"events"`
}

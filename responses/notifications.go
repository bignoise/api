package responses

import "bitbucket.org/bignoise/api"

type ResponseNotifications struct {
	Code          int                     `json:"code"`
	Notifications []api.NotificationEvent `json:"notifications"`
}

package api

import "strconv"

type GeoJson struct {
	Type        string    `json:"-"`
	Coordinates []float64 `json:"coordinates"`
}

func (geo *GeoJson) Print() string {
	return strconv.FormatFloat(geo.Coordinates[1], 'f', 6, 64) + "," + strconv.FormatFloat(geo.Coordinates[0], 'f', 6, 64)
}

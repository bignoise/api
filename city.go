package api

import "gopkg.in/mgo.v2/bson"

// City record
type City struct {
	ID       bson.ObjectId `json:"_id" bson:"_id,omitempty"`
	VkID     int           `json:"vk_id" bson:"vk_id"`
	Name     string        `json:"name"`
	Location GeoJson       `bson:"location,omitempty" json:"location"`
}

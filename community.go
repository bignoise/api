package api

import (
	"bitbucket.org/bignoise/api/vk"
	"gopkg.in/mgo.v2/bson"
)

type Community struct {
	ID               bson.ObjectId `json:"id" bson:"_id,omitempty"`
	VkID             uint32        `json:"vk_id" bson:"vk_id"`
	Name             string        `json:"name"`
	ScreenName       string        `json:"screen_name" bson:"screen_name"`
	City             int           `json:"city" bson:"city,omitempty"`
	Country          int           `json:"country" bson:"country,omitempty"`
	Description      string        `json:"description"`
	MembersCount     int           `json:"members_count" bson:"members_count"`
	Place            vk.Place      `json:"place"`
	Cover            vk.Cover      `json:"cover"`
	Photo            string        `json:"photo"`
	PhotoMedium      string        `json:"photo_medium" bson:"photo_medium"`
	PhotoBig         string        `json:"photo_big" bson:"photo_big"`
	Location         GeoJson       `bson:"location,omitempty" json:"location"`
	LocationAccuracy string        `json:"location_accuracy" bson:"location_accuracy"`
	Level            float64       `json:"level"`
}

type CommunityAnnounce struct {
	ID         bson.ObjectId `json:"id" bson:"_id,omitempty"`
	VkID       int64         `json:"vk_id" bson:"vk_id"`
	Name       string        `json:"name"`
	ScreenName string        `json:"screen_name" bson:"screen_name"`
	Place      vk.Place      `json:"place"`
	Photo      string        `json:"photo" bson:"photo_big"`
	Location   GeoJson       `bson:"location,omitempty" json:"location"`
	Level      float64       `json:"level"`
	Vote       Vote          `json:"vote"`
}

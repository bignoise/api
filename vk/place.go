package vk

// Place struct
type Place struct {
	ID           int     `json:"id"`
	Title        string  `json:"title"`
	Latitude     float64 `json:"latitude"`
	Longitude    float64 `json:"longitude"`
	Type         int     `json:"type"`
	City         int     `json:"city_id"`
	Country      int     `json:"country_id"`
	MetroStation int     `json:"metro_station_id"`
	Address      string  `json:"address"`
	WorkInfo     string  `json:"work_info_status"`
}

package api

// Vote record
type Vote struct {
	VkID     uint32 `bson:"vk_id" json:"vk_id"`
	UserID   uint32 `bson:"user_id"`
	Provider string `bson:"provider"`
	Vote     int    `bson:"vote"`
}

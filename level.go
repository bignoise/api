package api

// Level struct
type Level struct {
	VkID   uint32  `json:"vk_id" bson:"vk_id"`
	CityID int     `json:"city_id" bson:"city_id"`
	Source uint32  `json:"source"`
	Level  float64 `json:"level"`
}

package api

type NotificationEvent struct {
	Notification `bson:",inline"`
	Events       []EventAnnounce `json:"events" bson:"events"`
}
